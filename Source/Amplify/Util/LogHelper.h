// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/NoExportTypes.h"
#include "LogHelper.generated.h"

USTRUCT(BlueprintType)
struct FLogUpdate
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)
	FString Message;

	UPROPERTY(BlueprintReadOnly)
	FLinearColor MessageColor;

	FLogUpdate()
	{
		MessageColor = FLinearColor::White;
	}
};
/**
 * 
 */
UCLASS()
class ULogHelper : public UObject
{
	GENERATED_UCLASS_BODY()

	//returns a log message for a player spawning a character
	static FLogUpdate GetCharacterSpawnMessage(class AAmplifyPlayerState* Player, const FString& CharacterName);
	
	//returns a log message for a character action
	static FLogUpdate GetCharacterActionMessage(class AAmplifyPlayerState* Player, const FString& CharacterName, const FString& ActionName);
};
