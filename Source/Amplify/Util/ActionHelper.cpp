// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "../Props/Grid/GridCell.h"
#include "../Props/Character/BaseCharacter.h"
#include "ActionHelper.h"

UActionHelper::UActionHelper(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

TArray<AGridCell*> UActionHelper::GetSurroundCells(UWorld* World, AGridCell* Origin, int32 Radius)
{
	TArray<AGridCell*> Cells;

	if (Origin)
	{
		TArray<FHitResult> OutHits;
		FCollisionQueryParams Params;
		Params.AddIgnoredActor(Origin);
		World->SweepMultiByChannel(OutHits, Origin->GetActorLocation(), Origin->GetActorLocation(), FQuat::Identity, ECollisionChannel::ECC_Visibility, FCollisionShape::MakeBox(FVector(CellLength*Radius, CellLength*Radius, 0)), Params);

		if (OutHits.Num() > 0)
		{
			UE_LOG(LogTemp, Warning, TEXT("I have hits"))

				for (int32 Index = 0; Index < OutHits.Num();Index++)
				{
					AGridCell* GridCell = Cast<AGridCell>(OutHits[Index].GetActor());
					if (GridCell)
					{
						Cells.Add(GridCell);
					}
				}
		}
	}
	return Cells;
}

AGridCell* UActionHelper::FindClosestEnemy(UWorld* World, AGridCell* SourceLocation, int32 MyTeamId)
{
	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsOfClass(World, ABaseCharacter::StaticClass(), Actors);

	AGridCell* ClosestCell = nullptr;
	float ClosestCellDistance = 0.f;

	//TODO-Currently doesnt check if the closest character is on the other team.
	//TODO-Rather than using get all actors of class, just get opposing teams hero arrays

	for (int32 Index = 0; Index < Actors.Num(); Index++)
	{
		ABaseCharacter* Character = Cast<ABaseCharacter>(Actors[Index]);
		if (Character)
		{
			auto CharacterCell = Character->GetHeroCell();
			float CharacterCellDistance = GetDistanceBetweenCells(SourceLocation, CharacterCell);

			if (CharacterCell != SourceLocation)
			{
				if (ClosestCell == nullptr)
				{
					ClosestCell = CharacterCell;
					ClosestCellDistance = CharacterCellDistance;
				}
				else if (CharacterCellDistance < ClosestCellDistance)
				{
					ClosestCellDistance = CharacterCellDistance;
					ClosestCell = CharacterCell;
				}
			}
		}
	}

	return ClosestCell;
}

float UActionHelper::GetDistanceBetweenCells(AGridCell* Source, AGridCell* Destination)
{
	auto SourceLocation = Source->GetActorLocation();
	auto DestinationLocation = Destination->GetActorLocation();

	return FVector::Distance(SourceLocation, DestinationLocation);
}

AGridCell* UActionHelper::GetDesiredMove(UWorld* World, AGridCell* Destination, const TArray<AGridCell*>& PossibleCells)
{
	AGridCell* DesiredMoveCell = nullptr;
	float DesiredMoveCellDistance = 0.f;

	for (int32 Index = 0; Index < PossibleCells.Num(); Index++)
	{
		auto CurrentCell = PossibleCells[Index];
		float CurrentDistance = GetDistanceBetweenCells(CurrentCell, Destination);

		if (DesiredMoveCell == nullptr)
		{
			DesiredMoveCell = CurrentCell;
			DesiredMoveCellDistance = CurrentDistance;
		}
		else if (CurrentDistance < DesiredMoveCellDistance)
		{
			DesiredMoveCell = CurrentCell;
			DesiredMoveCellDistance = CurrentDistance;
		}
	}

	return DesiredMoveCell;
}


