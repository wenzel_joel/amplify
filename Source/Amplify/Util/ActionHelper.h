// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/NoExportTypes.h"
#include "ActionHelper.generated.h"

/**
 * 
 */
UCLASS()
class AMPLIFY_API UActionHelper : public UObject
{
	GENERATED_UCLASS_BODY()

public:
	//helper to get affected cells for a surround action. 
	static TArray<class AGridCell*> GetSurroundCells(UWorld* World, class AGridCell* Origin, int32 Radius);

	//Finds the closest enemy character from the source location
	static class AGridCell* FindClosestEnemy(UWorld* World, class AGridCell* SourceLocation, int32 MyTeamId);

	//Finds the closest valid move cell if attempting to go from source to destination.
	static class AGridCell* GetDesiredMove(UWorld* World, class AGridCell* Destination, const TArray<AGridCell*>& PossibleCells);

	//returns cell length
	FORCEINLINE static int32 GetCellLength() { return CellLength; }

private:
	static float GetDistanceBetweenCells(class AGridCell* Source, class AGridCell* Destination);
private:
	static const int32 CellLength = 200.f;
	
};
