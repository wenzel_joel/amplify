// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "../Player/AmplifyPlayerState.h"
#include "LogHelper.h"

ULogHelper::ULogHelper(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

FLogUpdate ULogHelper::GetCharacterSpawnMessage(AAmplifyPlayerState* Player, const FString& CharacterName)
{
	auto PlayerName = Player->PlayerInfo.PlayerName;
	
	FLogUpdate LogUpdate;
	LogUpdate.Message = FString(PlayerName + " spawned " + CharacterName);
	LogUpdate.MessageColor = Player->GetPlayerTeamInfo().TeamColor;
	
	return LogUpdate;
}

FLogUpdate ULogHelper::GetCharacterActionMessage(AAmplifyPlayerState* Player, const FString& CharacterName, const FString& ActionName)
{
	auto PlayerName = Player->PlayerInfo.PlayerName;

	FLogUpdate LogUpdate;
	LogUpdate.Message = FString(PlayerName + " used " + ActionName + " with "+CharacterName);
	LogUpdate.MessageColor = Player->GetPlayerTeamInfo().TeamColor;

	return LogUpdate;
}


