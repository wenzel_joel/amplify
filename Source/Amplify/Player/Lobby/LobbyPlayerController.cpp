// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "../../UI/Lobby/AmplifyLobbyWidget.h"
#include "LobbyPlayerController.h"

ALobbyPlayerController::ALobbyPlayerController()
{
	static::ConstructorHelpers::FClassFinder<UUserWidget> LobbyHUD(TEXT("/Game/UI/Lobby/LobbyMenu"));
	if (LobbyHUD.Class)
	{
		LobbyWidgetClass = LobbyHUD.Class;
	}
}

void ALobbyPlayerController::BeginPlay()
{
	Super::BeginPlay();

	CreateUI();		

}

void ALobbyPlayerController::CreateUI_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("Creating UI"))
	if (LobbyWidgetClass)
	{
		LobbyWidget = CreateWidget<UAmplifyLobbyWidget>(this, LobbyWidgetClass);
		if (LobbyWidget)
		{
			LobbyWidget->AddToViewport();
			LobbyWidget->SetOwningPlayer(this);
		}
		SetInputMode(FInputModeUIOnly());
		bShowMouseCursor = true;
	}
}

void ALobbyPlayerController::RefreshTeams_Implementation()
{
	LobbyWidget->RefreshTeams();
}

void ALobbyPlayerController::Client_ReadyGameplay_Implementation()
{
	if (LobbyWidget)
	{
		LobbyWidget->RemoveFromParent();
	}
	SetInputMode(FInputModeGameAndUI());
}