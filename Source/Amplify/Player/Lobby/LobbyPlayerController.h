// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "LobbyPlayerController.generated.h"

/**
 *
 */
UCLASS()
class AMPLIFY_API ALobbyPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ALobbyPlayerController();

	virtual void BeginPlay() override;

	UFUNCTION(Client, Reliable)
		void RefreshTeams();

	UFUNCTION(Client, Reliable)
		void Client_ReadyGameplay();

private:
	UPROPERTY()
		TSubclassOf<UUserWidget> LobbyWidgetClass;

	UPROPERTY()
		class UAmplifyLobbyWidget* LobbyWidget;

private:
	UFUNCTION(Client, Reliable)
		void CreateUI();
};
