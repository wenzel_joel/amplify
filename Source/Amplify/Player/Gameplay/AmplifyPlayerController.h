// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "../../Props/Actions/Action.h"
#include "Components/HeroSpawnComponent.h"
#include "Components/ActionManagerComponent.h"
#include "AmplifyPlayerController.generated.h"

UENUM(BlueprintType)
enum class EFocusState : uint8
{
	LOBBY_NO_TEAM UMETA(DisplayName = "Lobby No Team"),
	LOBBY_TEAM UMETA(DisplayName = "Lobby Team"),
	NOFOCUS UMETA(DisplayName = "No Focus"),
	PENDINGACTION UMETA(DisplayName = "Pending Action")
};

/**
 * 
 */
UCLASS()
class AMPLIFY_API AAmplifyPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AAmplifyPlayerController();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category ="Hero")
	UHeroSpawnComponent* HeroSpawnComponent = nullptr;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Action")
	UActionManagerComponent* ActionManager = nullptr;

public:
	virtual void BeginPlay() override;

	virtual void SetupInputComponent() override;

	//Called from the GameMode when the state is changed to match has started.
	//IE When the host clicks the start button
	virtual void HandleMatchHasStarted();

	//Action for character clicked
	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Action")
	void ActionClicked(FAction Action);

	UFUNCTION(Client, Reliable)
	void RefreshTeams();

	UFUNCTION(Client, Reliable)
	void EnableSpawnSelect();

	//Handle an update to the rotation.  Such as a new active team.
	UFUNCTION(Client, Reliable)
	void HandleTeamRotationUpdate(const TArray<int32>& NewRotation);

	//Handle action updates.  These get written out to the log window.
	UFUNCTION(Client, Reliable)
	void HandleEventLog(const FLogUpdate& LogEvent);

	//Handle gameplay notifications.  More than likely used to display UI updates
	//such as active team changing etc.
	UFUNCTION(Client, Reliable)
	void HandleNotification(const FString& Notification);

private:
	//Gameplay Movement
	void HandleClick();

	UFUNCTION(Server, Reliable, WithValidation)
	void NoTeamLobbyClick();

	UFUNCTION(Server, Reliable, WithValidation)
	void NoFocusClick(class AGridCell* GridCell);

	UFUNCTION(Server, Reliable, WithValidation)
	void LobbyClick(class AGridCell* GridCell);

	UFUNCTION(Server, Reliable, WithValidation)
	void PendingActionClick(class AGridCell* GridCell);

	UFUNCTION(Client, Reliable)
	void ReadyGameplay(const FTransform& SpawnTransform);

	//UI
	UFUNCTION(Client, Reliable)
	void CreateGameplayUI();

	UFUNCTION(Client, Reliable)
	void CreateLobbyUI();

	UFUNCTION(Client, Reliable)
	void DisplayCharacter(class ABaseCharacter* BaseCharacter);

	UFUNCTION(Client, Reliable)
	void ClearCharacter();

	UFUNCTION(Client, Reliable)
	void ToggleCharacterSpawn();

private:
	UPROPERTY(Replicated)
	EFocusState CurrentState = EFocusState::NOFOCUS;

	UPROPERTY()
	class AAmplifyPlayerState* MyPlayerState = nullptr;

	//Gameplay UI
	UPROPERTY()
	TSubclassOf<class UAmplifyGameplayUI> GameplayWidgetClass;

	UPROPERTY()
	UAmplifyGameplayUI* GameplayWidget;

	//Lobby UI
	UPROPERTY()
	TSubclassOf<UUserWidget> LobbyWidgetClass;

	UPROPERTY()
	class UAmplifyLobbyWidget* LobbyWidget;

	

	
};
