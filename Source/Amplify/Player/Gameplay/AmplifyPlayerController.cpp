// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "../../UI/Gameplay/AmplifyGameplayUI.h"
#include "../../UI/Lobby/AmplifyLobbyWidget.h"
#include "../../Props/Character/BaseCharacter.h"
#include "../../Props/Character/CharacterController.h"
#include "../../Props/Grid/GridCell.h"
#include "../AmplifyPlayerState.h"
#include "../../Props/Actions/Action.h"
#include "../../Props/Grid/SpawnableGridCell.h"
#include "../../Util/LogHelper.h"
#include "AmplifyPawn.h"
#include "AmplifyPlayerController.h"


AAmplifyPlayerController::AAmplifyPlayerController()
{
	HeroSpawnComponent = CreateDefaultSubobject<UHeroSpawnComponent>(TEXT("HeroSpawnComponent"));
	ActionManager = CreateDefaultSubobject<UActionManagerComponent>(TEXT("ActionManagerComponent"));

	static::ConstructorHelpers::FClassFinder<UUserWidget> GameplayUI(TEXT("/Game/UI/Gameplay/GameplayUI"));
	if (GameplayUI.Class)
	{
		GameplayWidgetClass = GameplayUI.Class;
	}
	static::ConstructorHelpers::FClassFinder<UUserWidget> LobbyUI(TEXT("/Game/UI/Lobby/LobbyMenu"));
	if (LobbyUI.Class)
	{
		LobbyWidgetClass = LobbyUI.Class;
	}
}

void AAmplifyPlayerController::BeginPlay()
{
	Super::BeginPlay();

	MyPlayerState = Cast<AAmplifyPlayerState>(PlayerState);

	CreateLobbyUI();
	CurrentState = EFocusState::LOBBY_NO_TEAM;
}

void AAmplifyPlayerController::CreateLobbyUI_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("Creating UI"))
		if (LobbyWidgetClass)
		{
			LobbyWidget = CreateWidget<UAmplifyLobbyWidget>(this, LobbyWidgetClass);
			if (LobbyWidget)
			{
				LobbyWidget->AddToViewport();
				LobbyWidget->SetOwningPlayer(this);
			}
			SetInputMode(FInputModeGameAndUI());
			bShowMouseCursor = true;
		}
}

void AAmplifyPlayerController::CreateGameplayUI_Implementation()
{
	if (LobbyWidget)
	{
		LobbyWidget->RemoveFromParent();
	}

	if (GameplayWidgetClass)
	{
		GameplayWidget = CreateWidget<UAmplifyGameplayUI>(this, GameplayWidgetClass);
		if (GameplayWidget)
		{
			GameplayWidget->AddToViewport();
			GameplayWidget->SetOwningPlayer(this);
		}
	}

	SetInputMode(FInputModeGameAndUI());
	bShowMouseCursor = true;
}

void AAmplifyPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AAmplifyPlayerController, CurrentState);
}

void AAmplifyPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("Select", IE_Pressed, this, &AAmplifyPlayerController::HandleClick);
	InputComponent->BindAction("ToggleCharacterSpawn", IE_Pressed, this, &AAmplifyPlayerController::ToggleCharacterSpawn);
}

void AAmplifyPlayerController::HandleMatchHasStarted()
{
	if (MyPlayerState)
	{
		auto AmplifyPawn = Cast<AAmplifyPawn>(GetPawn());
		if (AmplifyPawn)
		{
			//set pawn transform based on spawn selected.
			AmplifyPawn->ReadyGameplay(MyPlayerState->GetSpawnCell()->GetPlayerSpawnTransform());
		}
		CurrentState = EFocusState::NOFOCUS;
		CreateGameplayUI();
	}

}

void AAmplifyPlayerController::ReadyGameplay_Implementation(const FTransform& SpawnTransform)
{
	auto AmplifyPawn = Cast<AAmplifyPawn>(GetPawn());
	if (AmplifyPawn)
	{
		AmplifyPawn->bIsMovementEnabled = true;
		AmplifyPawn->SetActorTransform(SpawnTransform);
	}
}

void AAmplifyPlayerController::HandleClick()
{
	FHitResult HitResult;
	if (GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, HitResult))
	{
		AGridCell* NewFocusCell = Cast<AGridCell>(HitResult.GetActor());
		if (NewFocusCell)
		{
			if (CurrentState == EFocusState::NOFOCUS)
			{
				NoFocusClick(NewFocusCell);
			}
			else if (CurrentState == EFocusState::PENDINGACTION)
			{
				PendingActionClick(NewFocusCell);
			}
			else if (CurrentState == EFocusState::LOBBY_TEAM)
			{
				LobbyClick(NewFocusCell);
			}
			else if (CurrentState == EFocusState::LOBBY_NO_TEAM)
			{
				NoTeamLobbyClick();
			}
		}
	}
}

void AAmplifyPlayerController::EnableSpawnSelect_Implementation()
{
	if (LobbyWidget)
	{
		LobbyWidget->EnableSpawnSelect();
	}
}

void AAmplifyPlayerController::RefreshTeams_Implementation()
{
	LobbyWidget->RefreshTeams();
}

void AAmplifyPlayerController::ToggleCharacterSpawn_Implementation()
{
	if (GameplayWidget)
	{
		GameplayWidget->ToggleCharacterSpawn();
	}
}

void AAmplifyPlayerController::NoTeamLobbyClick_Implementation()
{
	if (MyPlayerState)
	{
		if (MyPlayerState->PlayerTeam >= 0)
		{
			CurrentState = EFocusState::LOBBY_TEAM;
			EnableSpawnSelect();
		}
	}
}

bool AAmplifyPlayerController::NoTeamLobbyClick_Validate()
{
	return true;
}

void AAmplifyPlayerController::LobbyClick_Implementation(AGridCell* GridCell)
{
	auto SpawnCell = Cast<ASpawnableGridCell>(GridCell);

	if (SpawnCell && SpawnCell->IsOwned())
	{
		//if this player owns it, unselect it.
		if (SpawnCell->GetOwningPlayer() == MyPlayerState)
		{
			SpawnCell->RemoveOwningPlayer();
			MyPlayerState->SetSpawnCell(nullptr);
		}
	}
	else if (SpawnCell && !SpawnCell->IsOwned())
	{
		//if player has a previous spawn cell, remove it so others can select it.
		if (MyPlayerState->GetSpawnCell())
		{
			MyPlayerState->GetSpawnCell()->RemoveOwningPlayer();
		}
		//set this player as the owner of the selected spawn cell.
		SpawnCell->SetOwningPlayer(MyPlayerState, true);
		MyPlayerState->SetSpawnCell(SpawnCell);
	}
}

bool AAmplifyPlayerController::LobbyClick_Validate(AGridCell* GridCell)
{
	return true;
}

void AAmplifyPlayerController::NoFocusClick_Implementation(AGridCell* GridCell)
{
	ActionManager->SetOriginCell(GridCell);
	if (GridCell->IsOccupied())
	{
		if (MyPlayerState->IsMyTurn())
		{
			const FAction& MoveAction = GridCell->GetOccupiedActor()->CharacterData.MoveAction;
			if (ActionManager)
			{
				ActionManager->PrepareAction(MoveAction);

				//Set state as PendingAction.  Pending Move Action*
				CurrentState = EFocusState::PENDINGACTION;
			}
		}
		DisplayCharacter(GridCell->GetOccupiedActor());
	}
}

bool AAmplifyPlayerController::NoFocusClick_Validate(AGridCell* GridCell)
{
	return true;
}

void AAmplifyPlayerController::PendingActionClick_Implementation(AGridCell* GridCell)
{
	if (ActionManager)
	{
		ActionManager->ExecuteAction();
		ActionManager->ClearOriginCell();
	}
	
	ClearCharacter();
	CurrentState = EFocusState::NOFOCUS;
}

bool AAmplifyPlayerController::PendingActionClick_Validate(AGridCell* GridCell)
{
	return true;
}

void AAmplifyPlayerController::ActionClicked_Implementation(FAction Action)
{
	UE_LOG(LogTemp, Warning, TEXT("Action Clicked"))
		if (MyPlayerState->IsMyTurn())
		{
			ActionManager->PrepareAction(Action);
		}
}

bool AAmplifyPlayerController::ActionClicked_Validate(FAction Action)
{
	return true;
}

void AAmplifyPlayerController::DisplayCharacter_Implementation(ABaseCharacter* BaseCharacter)
{
	if (GameplayWidget)
	{
		GameplayWidget->DisplayCharacter(BaseCharacter);
	}
}

void AAmplifyPlayerController::ClearCharacter_Implementation()
{
	if (GameplayWidget)
	{
		GameplayWidget->ClearCharacter();
	}
}

void AAmplifyPlayerController::HandleTeamRotationUpdate_Implementation(const TArray<int32>& NewRotation)
{

}

void AAmplifyPlayerController::HandleEventLog_Implementation(const FLogUpdate& LogEvent)
{
	if (GameplayWidget)
	{
		GameplayWidget->HandleLogUpdate(LogEvent);
	}
}

void AAmplifyPlayerController::HandleNotification_Implementation(const FString& Notification)
{
	if (GameplayWidget)
	{
		GameplayWidget->HandleNotification(Notification);
	}
	if (LobbyWidget)
	{
		UE_LOG(LogTemp, Warning, TEXT("TODO-UpdateLobbyNotification: %s "), *Notification)
	}
}