// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/DefaultPawn.h"
#include "AmplifyPawn.generated.h"

/**
 * 
 */
UCLASS()
class AMPLIFY_API AAmplifyPawn : public ADefaultPawn
{
	GENERATED_BODY()

public:
	AAmplifyPawn();

	UPROPERTY()
	UCameraComponent* CameraComponent;

public:
	virtual void BeginPlay() override;

	UFUNCTION(Client, Reliable)
	void ReadyGameplay(const FTransform& SpawnTransform);

public:
	UPROPERTY()
	bool bIsMovementEnabled = false;

protected:
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;

private:
	void MoveForward(float Value);
	void MoveRight(float Value);
	void StartAdjustPitch();
	void StopAdjustPitch();
	void AdjustControlRotation(float Value);

private:
	UPROPERTY()
	bool bIsAdjustingPitch = false;

	
};
