// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "../AmplifyPlayerState.h"
#include "../../Props/Grid/SpawnableGridCell.h"
#include "AmplifyPawn.h"

AAmplifyPawn::AAmplifyPawn()
{
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->SetupAttachment(GetRootComponent());

	bAddDefaultMovementBindings = false;
	bUseControllerRotationPitch = true;
	bReplicates = true;

	SetActorHiddenInGame(true);
}

void AAmplifyPawn::BeginPlay()
{
	Super::BeginPlay();
	/*auto MyPlayerState = Cast<AAmplifyPlayerState>(PlayerState);
	if (MyPlayerState)
	{
		if (MyPlayerState->GetPlayerStart())
		{
			SetActorTransform(MyPlayerState->GetPlayerStart()->GetActorTransform());
		}
	}*/
	
}

void AAmplifyPawn::ReadyGameplay_Implementation(const FTransform& SpawnTransform)
{
	bIsMovementEnabled = true;
	SetActorTransform(SpawnTransform);
	
	//Because use the controller rotation/pitch, we need to set that here.
	APlayerController* const PC = CastChecked<APlayerController>(Controller);
	PC->SetControlRotation(SpawnTransform.Rotator());
}

void AAmplifyPawn::SetupPlayerInputComponent(UInputComponent* InputComponent)
{
	InputComponent->BindAxis("MoveForward", this, &AAmplifyPawn::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AAmplifyPawn::MoveRight);
	InputComponent->BindAxis("LookUp", this, &AAmplifyPawn::AdjustControlRotation);
	InputComponent->BindAction("AdjustPitch", IE_Pressed, this, &AAmplifyPawn::StartAdjustPitch);
	InputComponent->BindAction("AdjustPitch", IE_Released, this, &AAmplifyPawn::StopAdjustPitch);
}

void AAmplifyPawn::StartAdjustPitch()
{
	if (bIsMovementEnabled)
	{
		bIsAdjustingPitch = true;
	}
}

void AAmplifyPawn::StopAdjustPitch()
{
	if (bIsMovementEnabled)
	{
		bIsAdjustingPitch = false;
	}
}

void AAmplifyPawn::AdjustControlRotation(float Value)
{
	if (bIsMovementEnabled)
	{
		if (bIsAdjustingPitch)
		{
			if (Value != 0.f && Controller && Controller->IsLocalPlayerController())
			{
				APlayerController* const PC = CastChecked<APlayerController>(Controller);
				PC->AddPitchInput(Value);
			}
		}
	}
}

void AAmplifyPawn::MoveForward(float Value)
{
	if (bIsMovementEnabled)
	{
		if (Value != 0.0f)
		{
			// add movement in that direction
			auto ForwardVector = GetActorForwardVector();
			AddMovementInput(FVector(ForwardVector.X, 0, 0), Value);
		}
	}
}

void AAmplifyPawn::MoveRight(float Value)
{
	if (bIsMovementEnabled)
	{
		if (Value != 0.0f)
		{
			// add movement in that direction
			AddMovementInput(GetActorRightVector(), Value);
		}
	}
}