// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "../../../Props/Character/CharacterData.h"
#include "HeroSpawnComponent.generated.h"

/*Component to allow the owner to spawn heroes into the world.
TODO-Eventually track hero stats here.
*/
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AMPLIFY_API UHeroSpawnComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHeroSpawnComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	UFUNCTION(BlueprintCallable, Category = "Heroes")
	FORCEINLINE TArray<class ABaseCharacter*>& GetSpawnedHeroes() { return SpawnedHeroes; }

	//Kills a hero in the game and removes from Array
	UFUNCTION(Server, Reliable, WithValidation)
	void KillHero(const class ABaseCharacter* Hero);

	//Spawns a Hero from CharacterData at the passed Transform
	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Heroes")
	void SpawnHeroAtTransform(const FCharacterData& CharacterData, const FTransform& SpawnTransform);

	//Spawns a Hero from CharacterData at PlayerState->SpawnCell
	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Heroes")
	void SpawnHeroAtPlayerSpawn(const FCharacterData& CharacterData);

	//Determines whether or not the owner is able to spawn the Hero
	UFUNCTION(BlueprintCallable, Category = "Heroes")
	bool CanSpawnHero(const FCharacterData& CharacterData);

private:
	UPROPERTY(Replicated)
	TArray<class ABaseCharacter*> SpawnedHeroes;
	
	UPROPERTY()
	AController* MyController;
};
