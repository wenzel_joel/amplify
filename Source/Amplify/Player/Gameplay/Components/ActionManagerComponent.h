// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "../../../Props/Actions/Action.h"
#include "ActionManagerComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AMPLIFY_API UActionManagerComponent : public UActorComponent
{
	GENERATED_BODY()

	DECLARE_DELEGATE_OneParam(FOnCursorHighlight, class AGridCell*);

public:	
	// Sets default values for this component's properties
	UActionManagerComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	//sets the current focus/origin
	void SetOriginCell(class AGridCell* InOrigin);

	//returns origin cell
	FORCEINLINE class AGridCell* GetOriginCell() { return OriginCell; }

	//clears the origin cell
	void ClearOriginCell();

	//returns current action of the component
	UFUNCTION(BlueprintCallable, Category = "Action")
	FORCEINLINE FAction& GetCurrentAction() { return CurrentAction; }

	//returns execution cells of the component
	UFUNCTION(BlueprintCallable, Category = "Action")
	FORCEINLINE TArray<class AGridCell*>& GetExecutionCells() { return ExecutionCells; }

	//returns valid execution cells of the prepare
	UFUNCTION(BlueprintCallable, Category = "Action")
	FORCEINLINE TArray<class AGridCell*>& GetValidCells() { return ValidCells; }

	//clears the execution cells and resets their color
	void ClearExecutionCells();

	//handle anything needed prior to execution.  IE display execution cells etc.
	void PrepareAction(const FAction& InCurrentAction);

	//execute the current action using the set ExecutionCells
	void ExecuteAction();

	//executes action against the passed cells.
	void ExecuteActionWithCells(const TArray<class AGridCell*>& Cells);

public:
	//delegate called when cursor highlights a new cell, passes NEW cell
	FOnCursorHighlight OnCursorEnter;

	//delegate called when cursor highlights a new cell, passes OLD cell
	FOnCursorHighlight OnCursorLeave;

private:
	//sets ExecutionCells for a surround action
	void HandleSurroundAction();

	//sets ExecutionCells for a single action.  
	void HandleSingleAction();

	//Sets the execution cell based on what cell is under the mouse cursor.
	UFUNCTION(Client, Reliable)
	void GetCellUnderCursor();

	UFUNCTION(Server, Reliable, WithValidation)
	void ProcessCellUnderCursor(class AGridCell* GridCell);

	//Processes an MOVE Type Execute
	bool HandleMoveExecute();

	//Processes an ATTACK Type Execute
	bool HandleAttackExecute();

	//Processes a HEAL Type Execute
	bool HandleHealExecute();

	//Client function to update cell colors.  Obviously this will only be seen by client.
	UFUNCTION(Client, Reliable)
	void UpdateCellColor(class AGridCell* Cell, float Value, const FLinearColor& Color);

private:
	UPROPERTY()
	class AController* MyController;

	UPROPERTY()
	class AAmplifyPlayerController* PlayerController;

	UPROPERTY()
	class AAmplifyPlayerState* PlayerState;

	UPROPERTY()
	bool bIsPlayer = true;

	//current action for the player
	UPROPERTY()
	FAction CurrentAction;

	//Origin/FocusCell for the action or player focus.
	UPROPERTY()
	class AGridCell* OriginCell;

	//contains the cells that this action will/should be executed against.
	UPROPERTY()
	TArray<class AGridCell*> ExecutionCells;

	//contains valid cells that can be selected for execution.
	UPROPERTY()
	TArray<class AGridCell*> ValidCells;

	/* Handle to manage the timer during cursor capture */
	UPROPERTY()
	FTimerHandle CursorTimerHandle;

	//colors
	const FLinearColor FocusColor = FLinearColor::Blue;
	const FLinearColor NormalColor = FLinearColor::Black;
	const FLinearColor PendingColor = FLinearColor::Green;
	
};
