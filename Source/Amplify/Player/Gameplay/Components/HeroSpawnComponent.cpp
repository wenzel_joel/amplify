// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "../../AmplifyPlayerState.h"
#include "../../../Props/Character/CharacterData.h"
#include "../../../Props/Character/BaseCharacter.h"
#include "../../../Props/Grid/SpawnableGridCell.h"
#include "HeroSpawnComponent.h"

UHeroSpawnComponent::UHeroSpawnComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UHeroSpawnComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UHeroSpawnComponent, SpawnedHeroes);
}

void UHeroSpawnComponent::BeginPlay()
{
	Super::BeginPlay();
	
	MyController = CastChecked<AController>(GetOwner());
}

void UHeroSpawnComponent::SpawnHeroAtTransform_Implementation(const FCharacterData& CharacterData, const FTransform& SpawnTransform)
{
	auto MyPlayerState = CastChecked<AAmplifyPlayerState>(MyController->PlayerState);
	auto SpawnedHero = GetWorld()->SpawnActor<ABaseCharacter>(CharacterData.CharacterClass, SpawnTransform);

	if (SpawnedHero)
	{
		SpawnedHero->SetOwningPlayer(MyPlayerState);
		SpawnedHero->SetCharacterData(CharacterData);
		SpawnedHero->SpawnDefaultController();

		SpawnedHeroes.Add(SpawnedHero);

		//set the spawn cell's occupant as the newly spawned hero.
		if (MyPlayerState->GetSpawnCell())
		{
			MyPlayerState->GetSpawnCell()->SetOccupiedActor(SpawnedHero);
			SpawnedHero->SetHeroCell(MyPlayerState->GetSpawnCell());
		}

		MyPlayerState->UpdateSpawnTokens(-(CharacterData.SpawnTokens));
		MyPlayerState->Server_HandlePlayerMove(ULogHelper::GetCharacterSpawnMessage(MyPlayerState, CharacterData.CharacterName));
	}
	
}

bool UHeroSpawnComponent::SpawnHeroAtTransform_Validate(const FCharacterData& CharacterData, const FTransform& SpawnTransform)
{
	return true;
}

void UHeroSpawnComponent::SpawnHeroAtPlayerSpawn_Implementation(const FCharacterData& CharacterData)
{
	auto MyPlayerState = CastChecked<AAmplifyPlayerState>(MyController->PlayerState);

	if (MyPlayerState->GetSpawnCell())
	{
		SpawnHeroAtTransform(CharacterData, MyPlayerState->GetSpawnCell()->GetHeroSpawnTransform());
	}
}

bool UHeroSpawnComponent::SpawnHeroAtPlayerSpawn_Validate(const FCharacterData& CharacterData)
{
	return true;
}

void UHeroSpawnComponent::KillHero_Implementation(const ABaseCharacter* Hero)
{

}

bool UHeroSpawnComponent::KillHero_Validate(const ABaseCharacter* Hero)
{
	return true;
}

bool UHeroSpawnComponent::CanSpawnHero(const FCharacterData& CharacterData)
{
	auto MyPlayerState = CastChecked<AAmplifyPlayerState>(MyController->PlayerState);

	if (CharacterData.SpawnTokens <= MyPlayerState->GetSpawnTokens())
	{
		return true;
	}
	return false;
}