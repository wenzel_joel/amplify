// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "Util/ActionHelper.h"
#include "../AmplifyPlayerController.h"
#include "../../AmplifyPlayerState.h"
#include "../../../Props/Grid/GridCell.h"
#include "../../../Props/Character/BaseCharacter.h"
#include "../../../Props/Character/CharacterController.h"
#include "ActionManagerComponent.h"


// Sets default values for this component's properties
UActionManagerComponent::UActionManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	bReplicates = true;
	// ...
}


// Called when the game starts
void UActionManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	MyController = Cast<AController>(GetOwner());
	PlayerState = CastChecked<AAmplifyPlayerState>(MyController->PlayerState);
	//set if the owner is an AI or Player.
	if (MyController->IsPlayerController())
	{
		bIsPlayer = true;
		PlayerController = Cast<AAmplifyPlayerController>(MyController);
	}
	else
	{
		bIsPlayer = false;
	}
}

void UActionManagerComponent::SetOriginCell(AGridCell* InOrigin)
{
	//if I am a player, highlight the cell
	if (bIsPlayer)
	{
		UpdateCellColor(InOrigin, 2.0f, FocusColor);

		//if i have a previous focus, revert that color
		ClearOriginCell();
	}
	OriginCell = InOrigin;
}

void UActionManagerComponent::ClearOriginCell()
{
	if (OriginCell && bIsPlayer)
	{
		UpdateCellColor(OriginCell, 0.0f, NormalColor);
	}
	OriginCell = nullptr;
}

void UActionManagerComponent::PrepareAction(const FAction& InCurrentAction)
{
	//set action and origin for this execution
	CurrentAction = InCurrentAction;

	//reset any prior execution cells if needed and remove any timers
	ClearExecutionCells();
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);

	switch (CurrentAction.Pattern)
	{
	case EActionPattern::SURROUND:
		HandleSurroundAction();
		break;
	case EActionPattern::SINGLE:
		HandleSingleAction();
		break;
	}
}

void UActionManagerComponent::HandleSurroundAction()
{
	//set the execution cells
	ExecutionCells = UActionHelper::GetSurroundCells(GetWorld(), OriginCell, CurrentAction.CellRadius);
	UE_LOG(LogTemp, Warning, TEXT("ExecutionCells: %i"), ExecutionCells.Num())
		if (bIsPlayer)
		{
			for (int32 Index = 0; Index < ExecutionCells.Num(); Index++)
			{
				UpdateCellColor(ExecutionCells[Index], 2.0f, PendingColor);
			}
		}
}

void UActionManagerComponent::HandleSingleAction()
{
	//get the valid execution cells
	ValidCells = UActionHelper::GetSurroundCells(GetWorld(), OriginCell, CurrentAction.CellRadius);
	//if it is a player controller, select cell based on mouse cursor
	if (bIsPlayer)
	{
		GetWorld()->GetTimerManager().SetTimer(CursorTimerHandle, this, &UActionManagerComponent::GetCellUnderCursor, .05f, true);
	}
	//else if AI
	else
	{

	}
}

void UActionManagerComponent::ProcessCellUnderCursor_Implementation(AGridCell* GridCell)
{
	//if it is a valid execute cell
	if (ValidCells.Contains(GridCell))
	{
		//Tell anyone listening what the cell is.
		//They may want to update the color etc.
		OnCursorEnter.ExecuteIfBound(GridCell);
		UpdateCellColor(GridCell, 2.0f, PendingColor);

		if (ExecutionCells.IsValidIndex(0))
		{
			//if i already have an execution cell and the new one is different
			//update it and call cursor leave delegate
			if (ExecutionCells[0] != GridCell)
			{
				OnCursorLeave.ExecuteIfBound(ExecutionCells[0]);
				UpdateCellColor(ExecutionCells[0], 0.0f, NormalColor);

				ExecutionCells[0] = GridCell;
			}
		}
		else
		{
			//if i dont already have one.  add it.
			ExecutionCells.Add(GridCell);
		}
	}
}

bool UActionManagerComponent::ProcessCellUnderCursor_Validate(AGridCell* GridCell)
{
	return true;
}

void UActionManagerComponent::GetCellUnderCursor_Implementation()
{
	FHitResult HitResult;
	if (PlayerController->GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, HitResult))
	{
		auto Actor = HitResult.GetActor();
		if (Actor)
		{
			auto GridCell = Cast<AGridCell>(Actor);
			if (GridCell)
			{
				ProcessCellUnderCursor(GridCell);
			}
		}
	}
}

bool UActionManagerComponent::HandleMoveExecute()
{
	UE_LOG(LogTemp, Warning, TEXT("Performing Move Action"))

	auto HeroController = Cast<ACharacterController>(OriginCell->GetOccupiedActor()->GetController());

	if (HeroController)
	{
		if (ExecutionCells.Num() > 0)
		{
			OriginCell->GetOccupiedActor()->SetHeroCell(ExecutionCells[0]);
			ExecutionCells[0]->SetOccupiedActor(OriginCell->GetOccupiedActor());

			HeroController->MoveCharacterToCell(ExecutionCells[0]);

			OriginCell->ClearOccupiedActor();
			
			return true;
		}
	}
	return false;
}

bool UActionManagerComponent::HandleAttackExecute()
{
	for (int32 Index = 0; Index < ExecutionCells.Num(); Index++)
	{
		//Play emitter for the action
		ExecutionCells[Index]->PlayEmitter(CurrentAction.ParticleEffect);
		if (ExecutionCells[Index]->IsOccupied())
		{
			//if the cell is occupied, apply damage
			ExecutionCells[Index]->GetOccupiedActor()->AffectHealth(-(CurrentAction.Magnitude));
		}
	}
	return true;
}

bool UActionManagerComponent::HandleHealExecute()
{
	UE_LOG(LogTemp, Warning, TEXT("Performing Heal Action"))
	return true;
}

void UActionManagerComponent::ExecuteAction()
{
	bool bWasExecuted = false;

	//clear all timers if any existing
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	if (CurrentAction.CellRadius > -1)
	{
		switch (CurrentAction.ActionType)
		{
		case EActionType::ATTACK:
			bWasExecuted = HandleAttackExecute();
			break;
		case EActionType::HEAL:
			bWasExecuted = HandleHealExecute();
			break;
		case EActionType::MOVE:
			bWasExecuted = HandleMoveExecute();
			break;
		}
		if (bWasExecuted)
		{
			PlayerState->Server_HandlePlayerMove(ULogHelper::GetCharacterActionMessage(PlayerState, TEXT("123"), TEXT("123")));
		}
	}

	ClearExecutionCells();
}

void UActionManagerComponent::ExecuteActionWithCells(const TArray<class AGridCell*>& Cells)
{
	//clear all timers if any existing
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
}

void UActionManagerComponent::ClearExecutionCells()
{
	if (bIsPlayer)
	{
		for (int32 Index = 0; Index < ExecutionCells.Num(); Index++)
		{
			UpdateCellColor(ExecutionCells[Index], 0.0f, NormalColor);
		}
	}
	ExecutionCells.Empty();
}

void UActionManagerComponent::UpdateCellColor_Implementation(AGridCell* Cell, float Value, const FLinearColor& Color)
{
	Cell->SetColor(Value, Color);
}