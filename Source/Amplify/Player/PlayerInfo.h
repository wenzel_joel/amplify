// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/UserDefinedStruct.h"
#include "../Props/Character/CharacterData.h"
#include "PlayerInfo.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct AMPLIFY_API FPlayerInfo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerInfo")
	FString PlayerName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerInfo")
	UTexture2D* PlayerAvatar = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerInfo")
	int32 PlayerCurrency;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerInfo")
	TArray<FCharacterData> GameCharacters;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerInfo")
	TArray<FCharacterData> OwnedCharacters;


	FPlayerInfo()
	{
	}

};
