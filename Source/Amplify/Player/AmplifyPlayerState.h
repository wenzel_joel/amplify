// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerState.h"
#include "PlayerInfo.h"
#include "../Game/Gameplay/AmplifyGameState.h"
#include "../Team/Team.h"
#include "AmplifyPlayerState.generated.h"


/**
 * 
 */
UCLASS()
class AMPLIFY_API AAmplifyPlayerState : public APlayerState
{
	GENERATED_BODY()
	
public:
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "PlayerInfo")
	FPlayerInfo PlayerInfo;

	UPROPERTY(Replicated, BlueprintReadWrite, EditAnywhere, Category = "PlayerTeam")
	int32 PlayerTeam = -1;

	UPROPERTY(BlueprintReadWrite, Category = "GameSave")
	class UAmplifySaveGame* MySaveGame;

public:
	virtual void CopyProperties(APlayerState* PlayerState) override;

	UFUNCTION(Server, Reliable, WithValidation)
	void SetController(class AController* InController);

	UFUNCTION(BlueprintCallable, Category = "Controller")
	FORCEINLINE AController* GetController() { return MyController; }

	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "GameSave")
	void SaveGame();

	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "GameSave")
	void LoadGame();

public:
	//Start Team Related Functions

	//Requests the game state to add this player to a team.
	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "PlayerTeam")
	void AddPlayerToTeam(int32 TeamId);

	UFUNCTION(BlueprintCallable, Category = "PlayerTeam")
	FTeam& GetPlayerTeamInfo() const;


public:
	//Start Turn Related Functions

	//Called from the game state to tell this player it is their turn.
	void HandleOnMyTurn(int32 NumActionsPerTurn);

	//Handles any logic that should be performed when it is no longer player turn.
	void HandleNotMyTurn();

	//Notifies GameState that this player has made a move.
	//NOTE - This can trigger a LONG list of function calls that are not technically Server RPCS
	//but because this starts the chain on the server, it should carry over there.
	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Turn")
	void Server_HandlePlayerMove(const FLogUpdate& LogUpdate);

	UFUNCTION(BlueprintCallable, Category = "Moves")
	FORCEINLINE bool IsMyTurn() { return bIsMyTurn; }

	UFUNCTION(BlueprintCallable, Category = "Moves")
	FORCEINLINE int32 GetMovesLeft() { return NumMovesLeft; }

public:
	//Start Spawn Related Functions
	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Spawn")
	void UpdateSpawnTokens(int32 DeltaTokens);

	UFUNCTION(BlueprintCallable, Category = "Spawn")
	FORCEINLINE int32 GetSpawnTokens() { return SpawnTokens; }
	
	UFUNCTION(Server, Reliable, WithValidation)
	void SetSpawnCell(class ASpawnableGridCell* InSpawnCell);

	UFUNCTION(Client, Reliable)
	void SetClientSpawnCell(class ASpawnableGridCell* InSpawnCell);

	FORCEINLINE class ASpawnableGridCell* GetSpawnCell() { return MySpawnCell; }

private:
	//Called by Server setter, each client has a player state for each player...
	//Not sure if this will update theirs as well or just the owning player client.
	//If the latter, move this to a NetMulticast?
	UFUNCTION(Client, Reliable)
	void SetClientController(class AController* InController);

private:
	UPROPERTY()
	class AController* MyController;

	//because this is private, this object needs to make sure they 
	//set it back to false when their turn is over.  
	//Couldnt decide if game state should be handling this.
	UPROPERTY(Replicated)
	bool bIsMyTurn = false;

	UPROPERTY(Replicated)
	int32 NumMovesLeft = 0;

	UPROPERTY(Replicated)
	int32 SpawnTokens = 10;

	UPROPERTY()
	class ASpawnableGridCell* MySpawnCell = nullptr;
};
