// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "../Game/AmplifySaveGame.h"
#include "../Game/AmplifyGameInstance.h"
#include "Gameplay/AmplifyPlayerController.h"
#include "../AI/AmplifyAIController.h"
#include "../Game/Gameplay/AmplifyGameState.h"
#include "../Props/Grid/SpawnableGridCell.h"
#include "../Team/Team.h"
#include "AmplifyPlayerState.h"


void AAmplifyPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AAmplifyPlayerState, PlayerInfo);
	DOREPLIFETIME(AAmplifyPlayerState, PlayerTeam);
	DOREPLIFETIME(AAmplifyPlayerState, bIsMyTurn);
	DOREPLIFETIME(AAmplifyPlayerState, NumMovesLeft);
	DOREPLIFETIME(AAmplifyPlayerState, SpawnTokens);
}

void AAmplifyPlayerState::SaveGame_Implementation()
{
	if (!MySaveGame)
	{
		MySaveGame = Cast<UAmplifySaveGame>(UGameplayStatics::CreateSaveGameObject(UAmplifySaveGame::StaticClass()));
	}
	MySaveGame->PlayerInfo = this->PlayerInfo;

	UAmplifyGameInstance* GameInstance = Cast<UAmplifyGameInstance>(GetWorld()->GetGameInstance());

	if (GameInstance)
	{
		UGameplayStatics::SaveGameToSlot(MySaveGame, GameInstance->PlayerSaveGameSlot, 0);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to Save Game due to null Game instance"))
	}

}

bool AAmplifyPlayerState::SaveGame_Validate()
{
	return true;
}

void AAmplifyPlayerState::LoadGame_Implementation()
{
	UAmplifyGameInstance* GameInstance = Cast<UAmplifyGameInstance>(GetWorld()->GetGameInstance());

	if (GameInstance)
	{
		UAmplifySaveGame* GameToLoad = Cast<UAmplifySaveGame>(UGameplayStatics::LoadGameFromSlot(GameInstance->PlayerSaveGameSlot, 0));
		if (GameToLoad)
		{
			//TODO  only set specific members that should be persisted.  IE dont set current character.
			this->PlayerInfo = GameToLoad->PlayerInfo;
		}
	}
}

bool AAmplifyPlayerState::LoadGame_Validate()
{
	return true;
}

void AAmplifyPlayerState::CopyProperties(APlayerState* PlayerState)
{
	Super::CopyProperties(PlayerState);

	AAmplifyPlayerState* MyPlayerState = Cast<AAmplifyPlayerState>(PlayerState);

	if (MyPlayerState)
	{
		MyPlayerState->PlayerInfo = PlayerInfo;
		MyPlayerState->PlayerTeam = PlayerTeam;
	}

}

void AAmplifyPlayerState::SetController_Implementation(AController* InController)
{
	MyController = InController;
	SetClientController(InController);
}

bool AAmplifyPlayerState::SetController_Validate(AController* InController)
{
	return true;
}

void AAmplifyPlayerState::SetClientController_Implementation(AController* InController)
{
	MyController = InController;
}

void AAmplifyPlayerState::AddPlayerToTeam_Implementation(int32 TeamId)
{
	auto GameState = Cast<AAmplifyGameState>(GetWorld()->GetGameState());
	if (GameState)
	{
		//if player is already assigned to a team, pass add with remove.
		if (PlayerTeam >= 0)
		{
			if (GameState->AddPlayerToTeam(this, TeamId, true, PlayerTeam))
			{	
				//on success, update player team
				PlayerTeam = TeamId;
			}
		}
		//else just add
		else
		{
			if (GameState->AddPlayerToTeam(this, TeamId, false, -1))
			{	
				//on success, update player team
				PlayerTeam = TeamId;
			}
		}
		
	}
}

bool AAmplifyPlayerState::AddPlayerToTeam_Validate(int32 TeamId)
{
	return true;
}

void AAmplifyPlayerState::Server_HandlePlayerMove_Implementation(const FLogUpdate& LogUpdate)
{
	auto GameState = CastChecked<AAmplifyGameState>(GetWorld()->GetGameState());
	GameState->PlayerMadeMove(this, LogUpdate);
	//decrement remaining moves
	NumMovesLeft--;

	//if moves is 0 then tell the game state my turn is over.
	if (NumMovesLeft <= 0)
	{
		HandleNotMyTurn();
	}
}

bool AAmplifyPlayerState::Server_HandlePlayerMove_Validate(const FLogUpdate& LogUpdate)
{
	return true;
}

void AAmplifyPlayerState::HandleOnMyTurn(int32 NumActionsPerTurn)
{
	NumMovesLeft = NumActionsPerTurn;
	bIsMyTurn = true;
	if (bIsABot)
	{
		auto AIController = CastChecked<AAmplifyAIController>(MyController);
		AIController->HandleAITurn();
	}
}

void AAmplifyPlayerState::HandleNotMyTurn()
{
	bIsMyTurn = false;
	auto GameState = CastChecked<AAmplifyGameState>(GetWorld()->GetGameState());
	GameState->PlayerTurnDone(this);
}

FTeam& AAmplifyPlayerState::GetPlayerTeamInfo() const
{
	auto GameState = CastChecked<AAmplifyGameState>(GetWorld()->GetGameState());
	
	return GameState->Teams[PlayerTeam];
}

void AAmplifyPlayerState::UpdateSpawnTokens_Implementation(int32 DeltaTokens)
{
	SpawnTokens += DeltaTokens;
}

bool AAmplifyPlayerState::UpdateSpawnTokens_Validate(int32 DeltaTokens)
{
	//TODO-probably could do some kind of validation here.
	return true;
}

void AAmplifyPlayerState::SetSpawnCell_Implementation(ASpawnableGridCell* InSpawnCell)
{
	MySpawnCell = InSpawnCell;
	SetClientSpawnCell(MySpawnCell);
}

bool AAmplifyPlayerState::SetSpawnCell_Validate(ASpawnableGridCell* InSpawnCell)
{
	return true;
}

void AAmplifyPlayerState::SetClientSpawnCell_Implementation(ASpawnableGridCell* InSpawnCell)
{
	MySpawnCell = InSpawnCell;
}