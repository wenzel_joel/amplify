// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "Engine.h"
#include "Action.generated.h"

UENUM(BlueprintType)
enum class EActionType : uint8
{
	ATTACK UMETA(DisplayName = "Attack"),
	HEAL UMETA(DisplayName = "Heal"),
	MOVE UMETA(DisplayName = "Move"),
	OTHER UMETA(DisplayName = "Other")
};

UENUM(BlueprintType)
enum class EActionPattern : uint8
{
	SINGLE UMETA(DisplayName = "Single"),
	SURROUND UMETA(DisplayName = "Surround"),
	LINE UMETA(DisplayName = "Line"),
	SELF UMETA(DisplayName = "Self")
};

/**
 * 
 */
USTRUCT(BlueprintType)
struct AMPLIFY_API FAction
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Setup")
	FString ActionName;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Setup")
	UTexture2D* ActionTexture;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Setup")
	FLinearColor ActionFontColor;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Setup")
	EActionType ActionType;

	//Base value of the damage or buff.  IE Magnitude of 100 on a damage attack means 
	//it will deal 100 damage assuming no buffs or debuffs.
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Setup")
	int32 Magnitude;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Setup")
	FString Description;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Setup")
	UParticleSystem* ParticleEffect;

	//Action pattern
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Setup")
	EActionPattern Pattern;

	//Signifies the maxiumum radius from origin that can be affected.
	//FOR EXAMPLE- a radius of 2 on a surround attack means all cells within a 2 cell radius are affected.
	//FOR EXAMPLE- a radius of 6 on a single means one cell within a 6 cell radius can be affected.
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Setup")
	int32 CellRadius = -1;

	FAction()
	{

	}	
	
};
