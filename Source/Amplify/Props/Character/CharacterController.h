// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "CharacterController.generated.h"

/**
 * 
 */
UCLASS()
class AMPLIFY_API ACharacterController : public AAIController
{
	GENERATED_BODY()
	
public:
	void MoveCharacterToCell(class AGridCell* GridCell);
	
	
};
