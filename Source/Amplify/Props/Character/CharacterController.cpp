// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "../Grid/GridCell.h"
#include "CharacterController.h"

void ACharacterController::MoveCharacterToCell(AGridCell* GridCell)
{
	FVector Location = GridCell->GetActorLocation();
	MoveToLocation(Location,1.0f,false,true);
}


