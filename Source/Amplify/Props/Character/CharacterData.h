// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/UserDefinedStruct.h"
#include "../Actions/Action.h"
#include "CharacterData.generated.h"

/**
 * Static data for a character that shouldn't change as gameplay progresses.
 */
USTRUCT(BlueprintType)
struct AMPLIFY_API FCharacterData
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	FString CharacterName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	TSubclassOf<class ABaseCharacter> CharacterClass =nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	UTexture2D* CharacterImage = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	int32 SpawnTokens;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	int32 MaxSpawned;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	int32 PurchaseCost;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	TArray<FAction> CharacterActions;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	FAction MoveAction;

	FCharacterData()
	{

	}
	
};
