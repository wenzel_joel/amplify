// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "../Actions/Action.h"
#include "CharacterData.h"
#include "BaseCharacter.generated.h"


/*
*
*/
UCLASS()
class AMPLIFY_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseCharacter();

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Setup")
	FCharacterData CharacterData;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//PROBABLY WONT USE THIS BUT LEAVING IT JUST IN CASE
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Set Owning Player.  Multicast so all connected players know who the owner is.
	//"Should" only be set once so replicated property is overkill.
	UFUNCTION(NetMulticast, Reliable)
	void SetOwningPlayer(class AAmplifyPlayerState* InOwningPlayer);

	//Return owning player of this character(IE the player that spawned the character and controlls it)
	UFUNCTION(BlueprintCallable, Category = "Player")
	FORCEINLINE class AAmplifyPlayerState* GetOwningPlayer() const { return OwningPlayer; }

	//Returns the grid cell that the character is standing on
	FORCEINLINE class AGridCell* GetHeroCell() { return MyGridCell; }

	//Sets the grid cell that the character is standing on
	FORCEINLINE void SetHeroCell(class AGridCell* GridCell) { MyGridCell = GridCell; }

	UFUNCTION(Server, Reliable, WithValidation)
	void AffectHealth(float DeltaHealth);

	UFUNCTION()
	FORCEINLINE bool IsDead() const { return bIsDead; }

	UFUNCTION(NetMulticast, Reliable)
	void SetCharacterData(const FCharacterData& InCharacterData);

protected:
	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Health")
	float CurrentHealth;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float MaxHealth;

	UPROPERTY(Replicated)
	bool bIsDead = false;

private:
	UPROPERTY()
	class AAmplifyPlayerState* OwningPlayer = nullptr;

	UPROPERTY()
	class AGridCell* MyGridCell;

	
};
