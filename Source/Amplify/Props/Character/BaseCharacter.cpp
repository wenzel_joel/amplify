// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "BaseCharacter.h"


// Sets default values
ABaseCharacter::ABaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	CurrentHealth = MaxHealth;
}

void ABaseCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ABaseCharacter, CurrentHealth);

	//TODO-Unreplicate IsDead.  Create NetMulticast function for KillCharacter or something.  
	DOREPLIFETIME(ABaseCharacter, bIsDead);
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ABaseCharacter::AffectHealth_Implementation(float DeltaHealth)
{
	CurrentHealth += DeltaHealth;
	if (CurrentHealth > MaxHealth)
	{
		CurrentHealth = MaxHealth;
	}
	else if (CurrentHealth <= 0)
	{
		bIsDead = true;
	}
}

bool ABaseCharacter::AffectHealth_Validate(float DeltaHealth)
{
	return true;
}

void ABaseCharacter::SetOwningPlayer_Implementation(AAmplifyPlayerState* InOwningPlayer)
{
	OwningPlayer = InOwningPlayer;
}

void ABaseCharacter::SetCharacterData_Implementation(const FCharacterData& InCharacterData)
{
	CharacterData = InCharacterData;
}