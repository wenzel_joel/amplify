// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Props/Grid/GridCell.h"
#include "../../Player/PlayerInfo.h"
#include "../../Game/Gameplay/AmplifyGameState.h"
#include "SpawnableGridCell.generated.h"

/**
 * 
 */
UCLASS()
class AMPLIFY_API ASpawnableGridCell : public AGridCell
{
	GENERATED_BODY()

	ASpawnableGridCell();
	
	UPROPERTY(EditAnywhere, Category = "Spawn")
	USceneComponent* SpawnTransform;

	UPROPERTY()
	class UWidgetComponent* NameWidget;

	UPROPERTY()
	UArrowComponent* SpawnRotation;

public:
	UPROPERTY(EditAnywhere, Category = "Spawn")
	int32 TeamId;

public:
	//Displays the player info on top of the cell.  Used during lobby when selecting spawns.
	UFUNCTION(NetMulticast, Reliable)
	void DisplayPlayerInfo(const struct FPlayerInfo& PlayerInfo);

	//Removes player info from the cell, called when player unselects spawn.
	UFUNCTION(NetMulticast, Reliable)
	void RemovePlayerInfo();

	UFUNCTION(Server, Reliable, WithValidation)
	void SetOwningPlayer(AAmplifyPlayerState* InOwningPlayer, bool bDisplayPlayerInfo);

	UFUNCTION(Server, Reliable, WithValidation)
	void RemoveOwningPlayer();

	FORCEINLINE AAmplifyPlayerState* GetOwningPlayer() const { return OwningPlayerState; }

	FORCEINLINE bool IsOwned() const { return bIsOwned; }

	//Returns the transform of the SpawnTransform component
	//..IE where the player pawn should spawn for gameplay.
	FTransform GetPlayerSpawnTransform();

	FTransform GetHeroSpawnTransform();

	

private:
	UPROPERTY()
	AAmplifyPlayerState* OwningPlayerState;

	UPROPERTY()
	bool bIsOwned = false;

	UPROPERTY()
	bool bIsDisplayingPlayerInfo = false;
	
	
	
};
