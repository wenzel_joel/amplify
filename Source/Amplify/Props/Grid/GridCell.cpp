// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "../Character/BaseCharacter.h"
#include "../../Player/PlayerInfo.h"

#include "GridCell.h"


// Sets default values
AGridCell::AGridCell()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);

	PanelMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PanelMesh"));
	PanelMesh->SetupAttachment(SceneComponent);

	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxCollision->SetupAttachment(PanelMesh);

	static::ConstructorHelpers::FObjectFinder<UStaticMesh> Mesh(TEXT("/Game/Meshes/Grid/GridPanel"));
	if (Mesh.Object)
	{
		PanelMesh->SetStaticMesh(Mesh.Object);
	}
	static::ConstructorHelpers::FObjectFinder<UMaterialInterface> Material(TEXT("/Game/Materials/GridMaterial"));
	if (Material.Object)
	{
		PanelMaterial = Material.Object;
	}

	//Network Settings
	bReplicates = true;
	SetNetUpdateTime(1.f);
}

void AGridCell::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AGridCell, CellColor);
	DOREPLIFETIME(AGridCell, Intensity);
	DOREPLIFETIME(AGridCell, bIsOccupied);
	DOREPLIFETIME(AGridCell, OccupiedActor);

}

// Called when the game starts or when spawned
void AGridCell::BeginPlay()
{
	Super::BeginPlay();

	if (PanelMesh)
	{
		PanelMesh->CreateDynamicMaterialInstance(0, PanelMaterial);
		OnRep_UpdatePanel();
	}

}

void AGridCell::SetOccupiedActor_Implementation(ABaseCharacter* Hero)
{
	if (!bIsOccupied)
	{
		OccupiedActor = Hero;
		bIsOccupied = true;
	}
}

bool AGridCell::SetOccupiedActor_Validate(ABaseCharacter* Hero)
{
	return true;
}

void AGridCell::ClearOccupiedActor_Implementation()
{
	if (OccupiedActor)
	{
		OccupiedActor = nullptr;
		bIsOccupied = false;
	}
}

bool AGridCell::ClearOccupiedActor_Validate()
{
	return true;
}

void AGridCell::PlayEmitter_Implementation(UParticleSystem* Particle)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Particle, GetActorLocation(), FRotator::ZeroRotator, true);
}

void AGridCell::SetColor(float Val, FLinearColor Color)
{
	if (PanelMesh)
	{
		auto Mat = Cast<UMaterialInstanceDynamic>(PanelMesh->GetMaterial(0));
		if (Mat)
		{
			Mat->SetScalarParameterValue(PanelBrightnessParam, Val);
			Mat->SetVectorParameterValue(PanelColorParam, Color);
		}
	}
}

void AGridCell::Server_SetColor_Implementation(float Val, FLinearColor Color)
{
	CellColor = Color;
	Intensity = Val;
	if (HasAuthority())
	{
		OnRep_UpdatePanel();
	}
}

bool AGridCell::Server_SetColor_Validate(float Val, FLinearColor Color)
{
	return true;
}

void AGridCell::OnRep_UpdatePanel()
{
	if (PanelMesh)
	{
		auto Mat = Cast<UMaterialInstanceDynamic>(PanelMesh->GetMaterial(0));
		if (Mat)
		{
			Mat->SetScalarParameterValue(PanelBrightnessParam, Intensity);
			Mat->SetVectorParameterValue(PanelColorParam, CellColor);
		}
	}
}

