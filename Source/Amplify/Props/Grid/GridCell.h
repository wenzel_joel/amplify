// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"

#include "GridCell.generated.h"

UCLASS()
class AMPLIFY_API AGridCell : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGridCell();

	UPROPERTY()
	USceneComponent* SceneComponent;

	UPROPERTY()
	UStaticMeshComponent* PanelMesh;

	UPROPERTY()
	UMaterialInterface* PanelMaterial;	

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	UBoxComponent* BoxCollision;

private:
	const FName PanelColorParam = "HighlightColor";	
	const FName PanelBrightnessParam = "HighlightValue";

	UPROPERTY(replicated)
	bool bIsOccupied = false;

	UPROPERTY(replicated)
	class ABaseCharacter* OccupiedActor;

	UPROPERTY(EditAnywhere, Category = "Setup")
	bool bIsTraversable = true;

	UPROPERTY(ReplicatedUsing = "OnRep_UpdatePanel")
	FLinearColor CellColor;

	UPROPERTY(ReplicatedUsing = "OnRep_UpdatePanel")
	float Intensity;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	FORCEINLINE bool IsOccupied() const { return bIsOccupied; }

	UFUNCTION(Server, Reliable, WithValidation)
	void SetOccupiedActor(class ABaseCharacter* Hero);

	UFUNCTION(Server, Reliable, WithValidation)
	void ClearOccupiedActor();

	FORCEINLINE ABaseCharacter* GetOccupiedActor() const { return OccupiedActor; }

	UFUNCTION()
	void OnRep_UpdatePanel();

	UFUNCTION(NetMulticast, Reliable)
	void PlayEmitter(UParticleSystem* Particle);

	//Local function to update the color of the cell
	void SetColor(float Val, FLinearColor Color);

	//Server RPC to update the color of the cell, this will replicate to all clients. 
	//This is only used in the game mode when showing the available spawn cells.
	//TODO-eventually remove completely and show spawn cells a different way.
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_SetColor(float Val, FLinearColor Color);	

	
	
};
