// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "Runtime/UMG/Public/Components/WidgetComponent.h"
#include "../../UI/Gameplay/CellWidget.h"
#include "../../Player/AmplifyPlayerState.h"
#include "SpawnableGridCell.h"


ASpawnableGridCell::ASpawnableGridCell()
{
	SpawnTransform = CreateDefaultSubobject<USceneComponent>(TEXT("SpawnTransform"));
	SpawnTransform->SetupAttachment(GetRootComponent());

	SpawnRotation = CreateDefaultSubobject<UArrowComponent>(TEXT("SpawnRotation"));
	SpawnRotation->SetupAttachment(SpawnTransform);

	NameWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("NameWidget"));
	NameWidget->SetupAttachment(PanelMesh);
	NameWidget->SetRelativeRotation(FRotator(90.f, 0, 0));
	NameWidget->SetDrawSize(FVector2D(200.f, 200.f));

	static::ConstructorHelpers::FClassFinder<UUserWidget> Widget(TEXT("/Game/UI/Gameplay/CellWidget"));
	if (Widget.Class)
	{
		NameWidget->SetWidgetClass(Widget.Class);
	}

}

void ASpawnableGridCell::DisplayPlayerInfo_Implementation(const FPlayerInfo& PlayerInfo)
{
	if (NameWidget)
	{
		auto Widget = Cast<UCellWidget>(NameWidget->GetUserWidgetObject());
		if (Widget)
		{
			Widget->SetPlayerText(PlayerInfo.PlayerName);
			Widget->ShowText();
			bIsDisplayingPlayerInfo = true;
		}
	}
}

void ASpawnableGridCell::RemovePlayerInfo_Implementation()
{
	if (NameWidget)
	{
		auto Widget = Cast<UCellWidget>(NameWidget->GetUserWidgetObject());
		if (Widget)
		{
			Widget->HideText();
			bIsDisplayingPlayerInfo = false;
		}
	}
}

void ASpawnableGridCell::SetOwningPlayer_Implementation(AAmplifyPlayerState* InOwningPlayer, bool bDisplayPlayerInfo) 
{
	OwningPlayerState = InOwningPlayer;
	bIsOwned = true;
	if (bDisplayPlayerInfo)
	{
		DisplayPlayerInfo(InOwningPlayer->PlayerInfo);
	}
}

bool ASpawnableGridCell::SetOwningPlayer_Validate(AAmplifyPlayerState* InOwningPlayer, bool bDisplayPlayerInfo)
{
	return true;
}

void ASpawnableGridCell::RemoveOwningPlayer_Implementation()
{
	OwningPlayerState = nullptr;
	bIsOwned = false;
	//remove player info from cell if valid.
	if (bIsDisplayingPlayerInfo)
	{
		RemovePlayerInfo();
	}
}

bool ASpawnableGridCell::RemoveOwningPlayer_Validate()
{
	return true;
}

FTransform ASpawnableGridCell::GetPlayerSpawnTransform()
{
	return SpawnRotation->GetComponentTransform();
}

FTransform ASpawnableGridCell::GetHeroSpawnTransform()
{
	auto ActorLocation = GetActorLocation();
	auto Rotation = SpawnRotation->GetComponentRotation();

	//88.f is the zed to spawn the character to land nicely on top of the cell.
	return FTransform(FRotator(0, Rotation.Yaw, 0), FVector(ActorLocation.X, ActorLocation.Y, 88.f));

}