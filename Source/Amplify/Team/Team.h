// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/UserDefinedStruct.h"
#include "Team.generated.h"

/**
 *
 */
USTRUCT(BlueprintType)
struct AMPLIFY_API FTeam
{
	GENERATED_USTRUCT_BODY()

	//team name, currently defaulted to red, blue, green, and yellow
	UPROPERTY(BlueprintReadWrite, Category = "TeamInfo")
	FString TeamName;

	//team color, used for showing team rotation during gameplay.
	UPROPERTY(BlueprintReadWrite, Category = "TeamInfo")
	FLinearColor TeamColor;

	//team avatar associated with team -- currently not supported
	UPROPERTY(BlueprintReadWrite, Category = "TeamInfo")
	UTexture2D* TeamAvatar = nullptr;

	//array of players belonging to this team
	UPROPERTY(BlueprintReadWrite, Category = "TeamInfo")
	TArray<class AAmplifyPlayerState*> TeamPlayers;

	//index in the game state array of teams
	UPROPERTY(BlueprintReadWrite, Category = "TeamInfo")
	int32 TeamId;

	FTeam()
	{

	}


};
