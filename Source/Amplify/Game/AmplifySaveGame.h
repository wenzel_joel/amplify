// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/SaveGame.h"
#include "../Player/PlayerInfo.h"
#include "AmplifySaveGame.generated.h"

/**
 * 
 */
UCLASS()
class AMPLIFY_API UAmplifySaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerInfo")
	struct FPlayerInfo PlayerInfo;
	
	
};
