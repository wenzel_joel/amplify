// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/UserDefinedStruct.h"
#include "UnrealNetwork.h"
#include "AmplifySession.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct AMPLIFY_API FAmplifySession
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly, Category = "SessionInfo")
	FString SessionName;

	UPROPERTY(BlueprintReadOnly, Category = "SessionInfo")
	FString SessionId;

	UPROPERTY(BlueprintReadOnly, Category = "SessionInfo")
	int32 CurrentPlayers;

	UPROPERTY(BlueprintReadOnly, Category = "SessionInfo")
	int32 MaxPlayers;

	FOnlineSessionSearchResult SearchResult;

	FAmplifySession()
	{

	}
	
	
	
	
};
