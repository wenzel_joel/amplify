// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "LobbyGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class AMPLIFY_API ALobbyGameModeBase : public AGameMode
{
	GENERATED_BODY()
	
public:
	ALobbyGameModeBase();
	
	virtual void PostLogin(APlayerController* NewPlayer) override;
	//virtual void Logout(AController* Exiting) override;

	UFUNCTION(BlueprintCallable, Category = "Gameplay")
	void LaunchGame(FString MapName);

private:
	TArray<class ALobbyPlayerController*> ConnectedPlayers;
};
