// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "../../Player/Lobby/LobbyPlayerController.h"
#include "../../Player/AmplifyPlayerState.h"
#include "../Gameplay/AmplifyGameState.h"
#include "LobbyGameModeBase.h"



ALobbyGameModeBase::ALobbyGameModeBase()
{
	PlayerControllerClass = ALobbyPlayerController::StaticClass();
	PlayerStateClass = AAmplifyPlayerState::StaticClass();
	GameStateClass = AAmplifyGameState::StaticClass();

	bUseSeamlessTravel = true;
}

void ALobbyGameModeBase::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	ALobbyPlayerController* PlayerController = Cast<ALobbyPlayerController>(NewPlayer);
	if (PlayerController)
	{
		ConnectedPlayers.Add(PlayerController);
		AAmplifyPlayerState* PlayerState = Cast<AAmplifyPlayerState>(NewPlayer->PlayerState);

		if (PlayerState)
		{
			PlayerState->LoadGame();
		}
	}
}

void ALobbyGameModeBase::LaunchGame(FString MapName)
{

	for (int32 Index = 0; Index < ConnectedPlayers.Num(); Index++)
	{
		auto LobbyController = Cast<ALobbyPlayerController>(ConnectedPlayers[Index]);
		if (LobbyController)
		{
			LobbyController->Client_ReadyGameplay();
		}
	}
	GetWorld()->ServerTravel(MapName);
}