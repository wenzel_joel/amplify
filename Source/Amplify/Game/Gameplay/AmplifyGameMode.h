// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "AmplifyGameMode.generated.h"

/**
 * 
 */
UCLASS()
class AMPLIFY_API AAmplifyGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
	AAmplifyGameMode();
	
	virtual void StartPlay() override;
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;

	virtual void InitGameState() override;

	virtual void HandleMatchIsWaitingToStart() override;
	virtual void HandleMatchHasStarted() override;

	virtual void InitSeamlessTravelPlayer(AController* NewController) override;
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exiting) override;

	virtual void HandleNewPlayer(AController* NewController);

	//Using this instead of ReadyToStartMatch as ReadyToStartMatch(I suspect) is called every
	//frame.  Wanted this call to be on demand.
	UFUNCTION(BlueprintCallable, Category = "Game")
	bool CanStartMatch();

	UFUNCTION(BlueprintNativeEvent, Category = "Game")
	bool ReadyToStartMatch();

	//function to spawn AI into the world
	class AAmplifyPlayerState* SpawnAI();

private:
	//Method to create default teams of Red, Blue, Green and Yellow
	void CreateDefaultTeams();

	//Method to get a random available spawn cell.  Used mostly for selected AI Spawn.
	class ASpawnableGridCell* GetAvailableSpawnCell();

private:
	UPROPERTY()
	class AAmplifyGameState* AmplifyGameState;

	UPROPERTY(EditAnywhere, Category = "Spawns")
	TArray<class ASpawnableGridCell*> SpawnCells;

	UPROPERTY()
	FTransform LobbySpawn;

	UPROPERTY()
	bool bNeedsDefaultTeams = true;
	
	UPROPERTY()
	int32 NumActionsPerTurn = 3;

};
