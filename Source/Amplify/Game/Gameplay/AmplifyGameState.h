// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameState.h"
#include "../../Team/Team.h"
#include "../../Util/LogHelper.h"
#include "AmplifyGameState.generated.h"

/**
 *
 */
UCLASS()
class AMPLIFY_API AAmplifyGameState : public AGameState
{
	GENERATED_BODY()

public:
	UPROPERTY(ReplicatedUsing = "OnRep_UpdateTeam", BlueprintReadOnly, EditAnywhere, Category = "Teams")
	TArray<FTeam> Teams;

	//Index of TeamRotation array representing Team whose turn it is.
	//Initialized to -1.  DONT CHANGE THIS
	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Teams")
	int32 TeamOnTheClock = -1;

	//Maximum number of players per team.  TODO-find a better spot for this.  Probablyl game mode
	UPROPERTY(BlueprintReadWrite, Category = "Teams")
	int32 MaxTeamPlayers = 4;

	//array of team indecies.  order of this array determines play order
	UPROPERTY(BlueprintReadOnly, Category = "Team")
	TArray<int32> TeamRotation;

public:
	//Adds a player to a team.  Only run if authority
	bool AddPlayerToTeam(class AAmplifyPlayerState* PlayerToAdd, int32 NewTeamId, bool bIsSwapping, int32 OldTeamId);

	//Removes a player from their old team.  Only run if authority
	void RemovePlayer(class AAmplifyPlayerState* PlayerToRemove, int32 TeamId);

	//Sets Random Team Rotation using the teams that have players. 
	//Should only be called after teams have been finalized.
	void SetTeamRotation();

	//Tells the active team it is their turn
	void UpdateRotation();

	//TeamRotation isn't updated enough to be considered for replication..
	//This method handles any one off needs to update the client on the rotation.
	UFUNCTION(NetMulticast, Reliable)
	void Clients_SetTeamRotation(const TArray<int32>& ServerTeamRotation);

	//Handles when a player makes a move.  Possibly send log updates to players
	void PlayerMadeMove(class AAmplifyPlayerState* Player, const FLogUpdate& Move);

	//Handles when a player if finished with their turn.  Should check other players on team
	//to see if they are done.  If all players done, move the rotation forward.
	void PlayerTurnDone(class AAmplifyPlayerState* Player);

	//Adds an AI player to the selected team.
	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Teams")
	void AddAIToTeam(int32 TeamId);

	//Removes an AI player from the selected team.
	//UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Teams")
	//void RemoveAIFromTeam(int32 TeamId);

private:
	UFUNCTION()
	void OnRep_UpdateTeam();

	bool ShouldUpdateRotation();

	//generic helper method to send a LOG MESSAGE to all player controllers
	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Log")
	void SendPlayerLogMessage(const struct FLogUpdate& Message);

	//generic helper method to send a NOTIFICATION to all player controllers
	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Log")
	void SendPlayerNotification(const FString& Message);

	
};
