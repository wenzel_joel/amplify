// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "../../Player/AmplifyPlayerState.h"
#include "AmplifyGameMode.h"
#include "../../Player/Gameplay/AmplifyPlayerController.h"
#include "AmplifyGameState.h"

void AAmplifyGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AAmplifyGameState, TeamOnTheClock);
	DOREPLIFETIME(AAmplifyGameState, Teams);
}

void AAmplifyGameState::AddAIToTeam_Implementation(int32 TeamId)
{
	if (Teams.IsValidIndex(TeamId))
	{
		FTeam& Team = Teams[TeamId];
		if (Team.TeamPlayers.Num() < MaxTeamPlayers)
		{
			auto GameMode = CastChecked<AAmplifyGameMode>(GetWorld()->GetAuthGameMode());
			auto AIPlayer = GameMode->SpawnAI();
			
			AIPlayer->AddPlayerToTeam(TeamId);
		}
	}
}

bool AAmplifyGameState::AddAIToTeam_Validate(int32 TeamId)
{
	return true;
}

bool AAmplifyGameState::AddPlayerToTeam(AAmplifyPlayerState* PlayerToAdd, int32 NewTeamId, bool bIsSwapping, int32 OldTeamId)
{
	if (HasAuthority())
	{
		if (Teams.IsValidIndex(NewTeamId))
		{
			FTeam& NewTeam = Teams[NewTeamId];

			//if NewTeam has space available
			if (NewTeam.TeamPlayers.Num() < MaxTeamPlayers)
			{
				//remove player from old team
				if (bIsSwapping)
				{
					RemovePlayer(PlayerToAdd, OldTeamId);
				}
				//add player to new team
				NewTeam.TeamPlayers.Add(PlayerToAdd);
				if (HasAuthority())
				{
					OnRep_UpdateTeam();
				}
				//return add success
				return true;
			}
		}
	}
	//return add failure
	return false;
}

void AAmplifyGameState::RemovePlayer(AAmplifyPlayerState* PlayerToRemove, int32 TeamId)
{
	if (HasAuthority())
	{
		if (Teams.IsValidIndex(TeamId))
		{
			Teams[TeamId].TeamPlayers.Remove(PlayerToRemove);
		}
	}
}

void AAmplifyGameState::OnRep_UpdateTeam()
{
	auto PlayerController = Cast<AAmplifyPlayerController>(GetWorld()->GetFirstPlayerController());
	if (PlayerController)
	{
		PlayerController->RefreshTeams();
	}
}

void AAmplifyGameState::SetTeamRotation()
{
	if (HasAuthority())
	{
		//clear rotation if it exists
		if (TeamRotation.Num() > 0)
		{
			TeamRotation.Empty();
		}
		//iterate through available teams
		for (int32 Index = 0; Index < Teams.Num(); Index++)
		{
			const FTeam& Team = Teams[Index];
			//if team has players, add it to the rotation
			if (Team.TeamPlayers.Num() > 0)
			{
				TeamRotation.Add(Team.TeamId);
			}
		}
		//randomize the rotation
		for (int32 Index = 0; Index < TeamRotation.Num(); Index++)
		{
			int32 RandomIndex = FMath::RandRange(0, TeamRotation.Num() - 1);
			TeamRotation.Swap(Index, RandomIndex);
		}
		Clients_SetTeamRotation(TeamRotation);
	}
}

void AAmplifyGameState::Clients_SetTeamRotation_Implementation(const TArray<int32>& ServerTeamRotation)
{
	TeamRotation = ServerTeamRotation;
}

void AAmplifyGameState::PlayerMadeMove(AAmplifyPlayerState* Player, const FLogUpdate& Move)
{
	//tell all the player controllers that they have a new log message to handle.
	SendPlayerLogMessage(Move);
}

void AAmplifyGameState::PlayerTurnDone(AAmplifyPlayerState* Player)
{
	//Maybe send out a log message that the player is done. 
	if (ShouldUpdateRotation())
	{
		UpdateRotation();
	}
}

bool AAmplifyGameState::ShouldUpdateRotation()
{
	//get the current team
	const FTeam& CurrentTeam = Teams[TeamRotation[TeamOnTheClock]];
	for (int32 Index = 0; Index < CurrentTeam.TeamPlayers.Num(); Index++)
	{
		auto PlayerState = CurrentTeam.TeamPlayers[Index];
		//if any player on the team still has a valid turn, then we shouldnt update
		if (PlayerState->IsMyTurn())
		{
			return false;
		}
	}
	return true;
}

void AAmplifyGameState::UpdateRotation()
{
	if (HasAuthority())
	{
		//TELL THE OLD TEAM IT IS NO LONGER THEIR TURN
		//READ THIS - player states are handling when their turn is done.  if that changes.. use the commented code below
		/*if (TeamOnTheClock > -1)
		{
			//get the old team.
			const FTeam& OldTeam = Teams[TeamRotation[TeamOnTheClock]];
			for (int32 Index = 0; Index < OldTeam.TeamPlayers.Num(); Index++)
			{
				auto PlayerState = OldTeam.TeamPlayers[Index];
				//tell all the players it isnt their turn anymore
				PlayerState->HandleNotMyTurn();
			}
		}*/

		//TELL THE NEW TEAM IT IS THEIR TURN

		//bump the rotation var
		TeamOnTheClock = ((TeamOnTheClock + 1) % TeamRotation.Num());

		//get active current team.
		const FTeam& NewTeam = Teams[TeamRotation[TeamOnTheClock]];

		//tell each player state on the team that it is their turn.
		for (int32 Index = 0; Index < NewTeam.TeamPlayers.Num(); Index++)
		{
			auto PlayerState = NewTeam.TeamPlayers[Index];
			//TODO-Replace 3 with the game mode number of turns.
			PlayerState->HandleOnMyTurn(3);
		}
		//Send a notification to the players
		
		SendPlayerNotification(FString(NewTeam.TeamName + " Is Up!"));

		//TODO-Send the players the new rotation
	}
}

void AAmplifyGameState::SendPlayerLogMessage_Implementation(const FLogUpdate& Message)
{
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		APlayerController* PlayerController = Iterator->Get();
		auto AmplifyController = Cast<AAmplifyPlayerController>(PlayerController);
		if (AmplifyController)
		{
			AmplifyController->HandleEventLog(Message);
		}
	}
}

bool AAmplifyGameState::SendPlayerLogMessage_Validate(const FLogUpdate& Message)
{
	return true;
}

void AAmplifyGameState::SendPlayerNotification_Implementation(const FString& Message)
{
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		APlayerController* PlayerController = Iterator->Get();
		auto AmplifyController = Cast<AAmplifyPlayerController>(PlayerController);
		if (AmplifyController)
		{
			AmplifyController->HandleNotification(Message);
		}
	}
}

bool AAmplifyGameState::SendPlayerNotification_Validate(const FString& Message)
{
	return true;
}