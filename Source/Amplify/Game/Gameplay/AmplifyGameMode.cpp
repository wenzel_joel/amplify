// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "Player/Gameplay/AmplifyPlayerController.h"
#include "Player/AmplifyPlayerState.h"
#include "Player/Gameplay/AmplifyPawn.h"
#include "AmplifyGameState.h"
#include "../../Props/Grid/SpawnableGridCell.h"
#include "../../AI/AmplifyAIPawn.h"
#include "../../AI/AmplifyAIController.h"
#include "AmplifyGameMode.h"

AAmplifyGameMode::AAmplifyGameMode()
{
	DefaultPawnClass = AAmplifyPawn::StaticClass();
	PlayerControllerClass = AAmplifyPlayerController::StaticClass();
	PlayerStateClass = AAmplifyPlayerState::StaticClass();
	GameStateClass = AAmplifyGameState::StaticClass();
}

void AAmplifyGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawnableGridCell::StaticClass(), Actors);
	for (int32 Index = 0; Index < Actors.Num(); Index++)
	{
		auto SpawnableCell = Cast<ASpawnableGridCell>(Actors[Index]);
		if (SpawnableCell)
		{
			SpawnCells.Add(SpawnableCell);
			SpawnableCell->Server_SetColor(2.0f, FLinearColor::White);
		}
	}

}

void AAmplifyGameMode::InitGameState()
{
	Super::InitGameState();

	AmplifyGameState = GetGameState<AAmplifyGameState>();
	if (AmplifyGameState)
	{
		if (bNeedsDefaultTeams)
		{
			CreateDefaultTeams();
		}
	}
}

void AAmplifyGameMode::StartPlay()
{
	Super::StartPlay();
	UE_LOG(LogTemp, Warning, TEXT("Starting Play"))
}

void AAmplifyGameMode::HandleMatchIsWaitingToStart()
{
	Super::HandleMatchIsWaitingToStart();
	UE_LOG(LogTemp, Warning, TEXT("MatchIsWaitingToStart"))
}

void AAmplifyGameMode::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();
	UE_LOG(LogTemp, Warning, TEXT("MatchHasStarted"))

		if (AmplifyGameState)
		{
			//create random team rotation
			AmplifyGameState->SetTeamRotation();

			//tell the connected player controllers that the match has started.
			for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
			{
				APlayerController* PlayerController = Iterator->Get();
				auto AmplifyController = Cast<AAmplifyPlayerController>(PlayerController);
				if (AmplifyController)
				{
					AmplifyController->HandleMatchHasStarted();
				}
			}

			//start the rotation
			AmplifyGameState->UpdateRotation();
		}
}

bool AAmplifyGameMode::CanStartMatch()
{
	//TODO-check that all player states are assigned a team.
	return true;
}

bool AAmplifyGameMode::ReadyToStartMatch_Implementation()
{
	return false;
}

void AAmplifyGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	HandleNewPlayer(NewPlayer);
}

void AAmplifyGameMode::InitSeamlessTravelPlayer(AController* NewController)
{
	Super::InitSeamlessTravelPlayer(NewController);
	HandleNewPlayer(NewController);
}

void AAmplifyGameMode::HandleNewPlayer(AController* NewController)
{
	UE_LOG(LogTemp, Warning, TEXT("HandlingNewPlayer"))
		auto PlayerState = Cast<AAmplifyPlayerState>(NewController->PlayerState);

	if (PlayerState)
	{
		PlayerState->LoadGame();
	}
	if (!IsMatchInProgress())
	{
		//Get First Player Spawn, should only be one(Lobby Spawn) and restart the player here.
		for (TActorIterator<APlayerStart> It(GetWorld()); It; ++It)
		{
			APlayerStart* PlayerStart = *It;
			RestartPlayerAtTransform(NewController, PlayerStart->GetActorTransform());
			break;
		}
	}
}

void AAmplifyGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);
	auto PlayerState = Cast<AAmplifyPlayerState>(Exiting->PlayerState);
	if (PlayerState)
	{
		PlayerState->SaveGame();
	}
}

void AAmplifyGameMode::CreateDefaultTeams()
{
	//initialize to 4 teams

	//red team
	FTeam RedTeam;
	RedTeam.TeamColor = FLinearColor::Red;
	RedTeam.TeamId = 0;
	RedTeam.TeamName = "Red Team";
	AmplifyGameState->Teams.Add(RedTeam);

	//blue team
	FTeam BlueTeam;
	BlueTeam.TeamColor = FLinearColor::Blue;
	BlueTeam.TeamId = 1;
	BlueTeam.TeamName = "Blue Team";
	AmplifyGameState->Teams.Add(BlueTeam);

	//green team
	FTeam GreenTeam;
	GreenTeam.TeamColor = FLinearColor::Green;
	GreenTeam.TeamId = 2;
	GreenTeam.TeamName = "Green Team";
	AmplifyGameState->Teams.Add(GreenTeam);

	//yellow team
	FTeam YellowTeam;
	YellowTeam.TeamColor = FLinearColor::Yellow;
	YellowTeam.TeamId = 3;
	YellowTeam.TeamName = "Yellow Team";
	AmplifyGameState->Teams.Add(YellowTeam);
}

AAmplifyPlayerState* AAmplifyGameMode::SpawnAI()
{
	AAmplifyPlayerState* PlayerState = nullptr;
	auto AIPawn = GetWorld()->SpawnActor<AAmplifyAIPawn>(AAmplifyAIPawn::StaticClass());
	if (AIPawn)
	{
		AIPawn->SpawnDefaultController();
		auto Controller = CastChecked<AAmplifyAIController>(AIPawn->GetController());

		//set the player name and controller reference.
		PlayerState = CastChecked<AAmplifyPlayerState>(Controller->PlayerState);
		PlayerState->PlayerInfo.PlayerName = FString("AIPlayer");
		PlayerState->SetController(AIPawn->GetController());

		//assign the AI a spawn cell if one exists.
		auto SpawnCell = GetAvailableSpawnCell();
		if (SpawnCell)
		{
			UE_LOG(LogTemp, Warning, TEXT("Setting AI Spawn Cell"))
			PlayerState->SetSpawnCell(SpawnCell);
			SpawnCell->SetOwningPlayer(PlayerState, true);
		}
	}
	return PlayerState;
}

ASpawnableGridCell* AAmplifyGameMode::GetAvailableSpawnCell()
{
	for (int32 Index = 0; Index < SpawnCells.Num(); Index++)
	{
		if (!SpawnCells[Index]->IsOwned())
		{
			return SpawnCells[Index];
		}
	}
	return nullptr;
}
