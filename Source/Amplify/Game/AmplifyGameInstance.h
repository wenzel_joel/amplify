// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "AmplifySession.h"
#include "AmplifyGameInstance.generated.h"

#define SETTING_SERVER_NAME FName(TEXT("SERVERNAME"))


/**
*
*/

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSessionsDelegate);

UCLASS()
class AMPLIFY_API UAmplifyGameInstance : public UGameInstance
{
	GENERATED_BODY()

public: //properties
	UPROPERTY(BlueprintReadOnly, Category = "GameSave")
		FString PlayerSaveGameSlot = "MySaveGameSlot";

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UUserWidget> MainMenuWidget;

	UPROPERTY(BlueprintReadWrite, Category = "PleaseWork")
		TArray<struct FAmplifySession> FoundSessions;

	UPROPERTY(BlueprintAssignable, Category = "Network")
		FSessionsDelegate OnSessionsFoundDelegate;

	UPROPERTY(BlueprintAssignable, Category = "Network")
		FSessionsDelegate OnNoSessionsFoundDelegate;

public://methods
	UAmplifyGameInstance(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, Category = "Setup")
		void Initialize();

	UFUNCTION(BlueprintCallable, Category = "Session")
		void LaunchLobby(int32 NumberOfPlayers, bool IsLan, FString ServerName);

	UFUNCTION(BlueprintCallable, Category = "Network")
		void FindOnlineGames(bool IsLAN);

	UFUNCTION(BlueprintCallable, Category = "Network")
		void JoinOnlineGame(FAmplifySession SessionInfo);

	UFUNCTION(BlueprintCallable, Category = "Network")
		void DestroySessionAndLeaveGame();

	bool HostSession(TSharedPtr<const FUniqueNetId> UserId, FString ServerName, bool bIsLAN, bool bIsPresence, int32 MaxNumPlayers);

	virtual void OnCreateSessionComplete(FName SessionName, bool bWasSuccessful);

	void OnStartOnlineGameComplete(FName SessionName, bool bWasSuccessful);

	void FindSessions(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, bool bIsLAN, bool bIsPresence);

	void OnFindSessionsComplete(bool bWasSuccessful);

	bool ConnectToSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, const FOnlineSessionSearchResult& SearchResult);

	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

	virtual void OnDestroySessionComplete(FName SessionName, bool bWasSuccessful);

private://properties
	UPROPERTY()
	UUserWidget* MainMenuRef;

	TSharedPtr<class FOnlineSessionSettings> SessionSettings;
	TSharedPtr<class FOnlineSessionSearch> SessionSearch;

	//Delegates and handles
	FDelegateHandle OnCreateSessionCompleteDelegateHandle;
	FOnCreateSessionCompleteDelegate OnCreateSessionCompleteDelegate;

	FDelegateHandle OnStartSessionCompleteDelegateHandle;
	FOnStartSessionCompleteDelegate OnStartSessionCompleteDelegate;

	FDelegateHandle OnFindSessionsCompleteDelegateHandle;
	FOnFindSessionsCompleteDelegate OnFindSessionsCompleteDelegate;

	FDelegateHandle OnJoinSessionCompleteDelegateHandle;
	FOnJoinSessionCompleteDelegate OnJoinSessionCompleteDelegate;

	FDelegateHandle OnDestroySessionCompleteDelegateHandle;
	FOnDestroySessionCompleteDelegate OnDestroySessionCompleteDelegate;





};
