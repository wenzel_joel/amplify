// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "../Player/AmplifyPlayerState.h"
#include "../Player/Gameplay/Components/HeroSpawnComponent.h"
#include "../Player/Gameplay/Components/ActionManagerComponent.h"
#include "../Props/Character/BaseCharacter.h"
#include "../Props/Grid/GridCell.h"
#include "../Util/ActionHelper.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "AmplifyAIController.h"

AAmplifyAIController::AAmplifyAIController()
{
	bWantsPlayerState = true;
	
	HeroSpawnComponent = CreateDefaultSubobject<UHeroSpawnComponent>(TEXT("HeroSpawnComponent"));

	ActionManager = CreateDefaultSubobject<UActionManagerComponent>(TEXT("ActionManagerComponent"));

	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));
	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));

	static::ConstructorHelpers::FObjectFinder<UBehaviorTree> Tree(TEXT("/Game/Player/AI/BT_EasyAI"));
	if (Tree.Object)
	{
		BehaviorTree = Tree.Object;
	}

}

void AAmplifyAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);	
}

void AAmplifyAIController::BeginPlay()
{
	Super::BeginPlay();
	MyPlayerState = CastChecked<AAmplifyPlayerState>(PlayerState);
	if (BehaviorTree)
	{
		BlackboardComp->InitializeBlackboard(*BehaviorTree->BlackboardAsset);
	}
}

void AAmplifyAIController::HandleAITurn_Implementation()
{
	if (BehaviorTree)
	{
		BehaviorComp->StartTree(*BehaviorTree);
	}
}

bool AAmplifyAIController::HandleAITurn_Validate()
{
	return true;
}

void AAmplifyAIController::Move_SpawnHero_Implementation(const FCharacterData& CharacterData)
{
	if (HeroSpawnComponent)
	{
		if (HeroSpawnComponent->CanSpawnHero(CharacterData))
		{
			HeroSpawnComponent->SpawnHeroAtPlayerSpawn(CharacterData);
		}
	}
}

bool AAmplifyAIController::Move_SpawnHero_Validate(const FCharacterData& CharacterData)
{
	return true;
}

void AAmplifyAIController::Move_UseAction_Implementation()
{
	TArray<ABaseCharacter*>& Heroes = HeroSpawnComponent->GetSpawnedHeroes();
	if (Heroes.Num() > 0)
	{
		FAction& Action = Heroes[0]->CharacterData.CharacterActions[0];
		ActionManager->SetOriginCell(Heroes[0]->GetHeroCell());
		ActionManager->PrepareAction(Action);
		ActionManager->ExecuteAction();
	}
}

bool AAmplifyAIController::Move_UseAction_Validate()
{
	return true;
}

void AAmplifyAIController::Move_MoveTowardsEnemy_Implementation()
{
	if (HeroSpawnComponent->GetSpawnedHeroes().IsValidIndex(0))
	{
		auto MyHero = HeroSpawnComponent->GetSpawnedHeroes()[0];
		auto ClosestEnemy = UActionHelper::FindClosestEnemy(GetWorld(), MyHero->GetHeroCell(), 0);
		if (ClosestEnemy)
		{
			UE_LOG(LogTemp, Warning, TEXT("Closest Enemy: %s "), *ClosestEnemy->GetName())

			FAction& Move = MyHero->CharacterData.MoveAction;
			ActionManager->SetOriginCell(MyHero->GetHeroCell());
			ActionManager->PrepareAction(Move);

			auto MyMoveCell = UActionHelper::GetDesiredMove(GetWorld(), ClosestEnemy, ActionManager->GetValidCells());
			
			UE_LOG(LogTemp, Warning, TEXT("My Move Cell: %s"), *MyMoveCell->GetName())
		}
		//TODO change log message
		MyPlayerState->Server_HandlePlayerMove(ULogHelper::GetCharacterActionMessage(MyPlayerState, TEXT(" "), TEXT(" ")));
	}
	
}

bool AAmplifyAIController::Move_MoveTowardsEnemy_Validate()
{
	return true;
}
