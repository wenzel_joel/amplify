// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "AmplifyAIPawn.generated.h"

UCLASS()
class AMPLIFY_API AAmplifyAIPawn : public APawn
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAmplifyAIPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
