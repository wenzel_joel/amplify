// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "AmplifyAIController.h"
#include "AmplifyAIPawn.h"


// Sets default values
AAmplifyAIPawn::AAmplifyAIPawn()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AIControllerClass = AAmplifyAIController::StaticClass();
}

// Called when the game starts or when spawned
void AAmplifyAIPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAmplifyAIPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

