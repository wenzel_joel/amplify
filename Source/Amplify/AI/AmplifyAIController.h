// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "../Props/Character/CharacterData.h"
#include "AmplifyAIController.generated.h"

/**
 * 
 */
UCLASS()
class AMPLIFY_API AAmplifyAIController : public AAIController
{
	GENERATED_BODY()


public:
	AAmplifyAIController();

	UPROPERTY(BlueprintReadWrite, Category = "Spawn")
	class UHeroSpawnComponent* HeroSpawnComponent;

	UPROPERTY(BlueprintReadWrite, Category = "Action")
	class UActionManagerComponent* ActionManager;

	//Behavior
	UPROPERTY(transient)
	UBlackboardComponent* BlackboardComp;

	UPROPERTY(transient)
	class UBehaviorTreeComponent* BehaviorComp;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	UBlackboardData* BlackboardAsset;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	UBehaviorTree* BehaviorTree;

public:

	virtual void Tick(float DeltaSeconds) override;

	//Called when AI Turn begins
	UFUNCTION(Server, Reliable, WithValidation)
	virtual void HandleAITurn();

	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Turn")
	virtual void Move_SpawnHero(const FCharacterData& CharacterData);

	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Turn")
	virtual void Move_UseAction();

	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Turn")
	virtual void Move_MoveTowardsEnemy();

protected:
	virtual void BeginPlay() override;
	
private:
	UPROPERTY()
	class AAmplifyPlayerState* MyPlayerState;
};
