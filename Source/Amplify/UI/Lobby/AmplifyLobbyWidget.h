// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "AmplifyLobbyWidget.generated.h"

/**
 * 
 */
UCLASS()
class AMPLIFY_API UAmplifyLobbyWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent, Category = "Teams")
	void RefreshTeams();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Spawns")
		void EnableSpawnSelect();
	
	
};
