// Fill out your copyright notice in the Description page of Project Settings.

#include "Amplify.h"
#include "AmplifyGameplayUI.h"


void UAmplifyGameplayUI::UpdateActionList_Implementation(const TArray<FAction>& Actions)
{
	//overridden in blueprint
}

void UAmplifyGameplayUI::ClearActionList_Implementation()
{
	//overridden in blueprint
}
