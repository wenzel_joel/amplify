// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "../../Props/Actions/Action.h"
#include "AmplifyGameplayUI.generated.h"


/**
 * 
 */
UCLASS()
class AMPLIFY_API UAmplifyGameplayUI : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Actions")
	void UpdateActionList(const TArray<FAction>& Actions);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Actions")
	void ClearActionList();

	UFUNCTION(BlueprintImplementableEvent)
	void DisplayCharacter(ABaseCharacter* CharacterToDisplay);

	UFUNCTION(BlueprintImplementableEvent)
	void ClearCharacter();

	//Messages to display in the log window.  Character Actions, spawning heroes etc.
	UFUNCTION(BlueprintImplementableEvent)
	void HandleLogUpdate(const struct FLogUpdate& LogUpdate);

	//Messages to display on center screen.  IMPORTANT events such as new active team or team winning the game.
	UFUNCTION(BlueprintImplementableEvent)
	void HandleNotification(const FString& Notification);

	//Updates the team rotation.
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateTeamRotation(const TArray<int32>& NewRotation);

	//Toggles the Character Spawn screen.
	UFUNCTION(BlueprintImplementableEvent)
	void ToggleCharacterSpawn();
	
	
	
};
