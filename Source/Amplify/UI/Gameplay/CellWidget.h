// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "CellWidget.generated.h"

/**
 * 
 */
UCLASS()
class AMPLIFY_API UCellWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:	
	UFUNCTION(BlueprintImplementableEvent, Category = "Text")
	void SetColor(const FLinearColor& ColorToSet);

	UFUNCTION(BlueprintImplementableEvent, Category = "Text")
	void HideText();

	UFUNCTION(BlueprintImplementableEvent, Category = "Text")
	void ShowText();

	UFUNCTION(BlueprintImplementableEvent, Category = "Text")
	void SetPlayerText(const FString& TextToSet);
};
